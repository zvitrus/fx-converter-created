package me.pcicenas.currency.fxconverter.services;

import me.pcicenas.currency.fxconverter.dto.ConverterResponseDTO;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

/**
 * A test for {@link RateServiceImpl}.
 *
 * @author pcicenas
 * @since 2020-06
 */
public class RateServiceImplTest {

    private static final String MAIN_CURRENCY_CD = "EUR";
    private static final String AMOUNT = "1";
    private static final String MONEY_CURRENCY_CD = "DKK";
    private static final String NEGATIVE_AMOUNT = "-1";
    private static final String ERROR_MESSAGE_INVALID_AMOUNT = "The amount must not be empty or negative";
    private static final String INVALID_CURRENCY_CD = "invalidCurrencyCd";
    private static final String ERROR_MESSAGE_INVALID_CURRENCY_CD = "A currency type is unknown or has not been provided.";

    private RateServiceImpl rateService;

    /**
     * @author pcicenas
     * @since 2020-06
     */
    @Before
    public void setUp() {
        rateService = new RateServiceImpl();
        rateService.setRatesOfCurrencies(prepareRatesOfCurrencies());
    }

    /**
     * @author pcicenas
     * @since 2020-06
     */
    @Test
    public void shouldCalculateAmount() {
        // given
        BigDecimal rate = retrieveRate();

        // when
        ConverterResponseDTO converterResponseDTO = rateService.calculateAmount(MAIN_CURRENCY_CD, MONEY_CURRENCY_CD,
                AMOUNT);

        // then
        assertThat(converterResponseDTO.getErrorMessages(), nullValue());
        assertThat(converterResponseDTO.getValue(), equalTo(new BigDecimal(AMOUNT).multiply(rate).toString()));
    }

    /**
     * @author pcicenas
     * @since 2020-06
     */
    @Test
    public void shouldNotCalculateAmountWithANegativeInput() {
        // when
        ConverterResponseDTO converterResponseDTO = rateService.calculateAmount(MAIN_CURRENCY_CD, MONEY_CURRENCY_CD,
                NEGATIVE_AMOUNT);

        // then
        assertThat("There must be an error message present.", converterResponseDTO.getErrorMessages(),
                notNullValue());
        assertThat("There should be no value returned.", converterResponseDTO.getValue(), nullValue());
        assertThat("The error message is wrong.", converterResponseDTO.getErrorMessages().get(0),
                equalTo(ERROR_MESSAGE_INVALID_AMOUNT));
    }

    /**
     * @author pcicenas
     * @since 2020-06
     */
    @Test
    public void shouldCalculateRate() {
        //given
        String rate = retrieveRate().toString();

        // when
        ConverterResponseDTO converterResponseDTO = rateService.calculateAmount(MAIN_CURRENCY_CD, MONEY_CURRENCY_CD,
                AMOUNT);

        // then
        assertThat(converterResponseDTO.getErrorMessages(), nullValue());
        assertThat(converterResponseDTO.getValue(), equalTo(rate));
    }

    /**
     * @author pcicenas
     * @since 2020-06
     */
    @Test
    public void shouldNotCalculateRateWithInvalidCurrencyCd() {
        // when
        ConverterResponseDTO converterResponseDTO = rateService.calculateAmount(MAIN_CURRENCY_CD, INVALID_CURRENCY_CD,
                AMOUNT);

        // then
        assertThat("There must be an error message present.", converterResponseDTO.getErrorMessages(),
                notNullValue());
        assertThat("There should be no value returned.", converterResponseDTO.getValue(), nullValue());
        assertThat("The error message is wrong.", converterResponseDTO.getErrorMessages().get(0),
                equalTo(ERROR_MESSAGE_INVALID_CURRENCY_CD));
    }

    /**
     * A helper method used for testing purposes only.
     *
     * @return An instance of {@link Map} with prefilled data.
     * @author pcicenas
     * @since 2020-06
     */
    private Map<String, String> prepareRatesOfCurrencies() {
        Map<String, String> ratesOfCurrencies = new HashMap<>();
        ratesOfCurrencies.put("DKK", "100.00");
        ratesOfCurrencies.put("EUR", "743.94");
        ratesOfCurrencies.put("USD", "663.11");

        return ratesOfCurrencies;
    }

    /**
     * A helper method used for testing purposes only.
     *
     * @return An instance of {@link BigDecimal} containing a calculated rate.
     * @author pcicenas
     * @since 2020-06
     */
    protected BigDecimal retrieveRate() {
        return new BigDecimal(rateService.getRatesOfCurrencies().get(MAIN_CURRENCY_CD))
                .divide(new BigDecimal(rateService.getRatesOfCurrencies()
                        .get(MONEY_CURRENCY_CD)), 5, RoundingMode.HALF_DOWN);
    }
}