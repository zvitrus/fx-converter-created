package me.pcicenas.currency.fxconverter.controllers;

import me.pcicenas.currency.fxconverter.services.RateService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;

/**
 * A test for {@link ConverterController}.
 *
 * @author pcicenas
 * @since 2020-06
 */
@RunWith(MockitoJUnitRunner.class)
public class ConverterControllerTest {

    private static final String MAIN_CURRENCY_CD = "EUR";
    private static final String AMOUNT = "1";
    private static final String MONEY_CURRENCY_CD = "DKK";

    @Mock
    private RateService rateService;
    @InjectMocks
    private ConverterController converterController;

    /**
     * @author pcicenas
     * @since 2020-06
     */
    @Test
    public void shouldConvertAmount() {
        // when
        converterController.convertAmount(MAIN_CURRENCY_CD, MONEY_CURRENCY_CD, AMOUNT);

        // then
        verify(rateService).calculateAmount(anyString(), anyString(), anyString());
    }

    /**
     * @author pcicenas
     * @since 2020-06
     */
    @Test
    public void shouldRetrieveRate() {
        // when
        converterController.retrieveRate(MAIN_CURRENCY_CD, MONEY_CURRENCY_CD);

        // then
        verify(rateService).calculateRate(anyString(), anyString());
    }
}