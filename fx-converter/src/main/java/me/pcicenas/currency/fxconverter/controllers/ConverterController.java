package me.pcicenas.currency.fxconverter.controllers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import me.pcicenas.currency.fxconverter.dto.ConverterResponseDTO;
import me.pcicenas.currency.fxconverter.services.RateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * A REST controller implementation responsible for operations related to money conversion.
 *
 * @author pcicenas
 * @since 2020-06
 */
@RestController
@RequestMapping("/converter")
public class ConverterController {

    @Autowired()
    private RateService rateService;

    /**
     * Converts money from a given currency to another given currency.
     *
     * @param mainCurrencyCd  the code of a currency the money is to be converted from.
     * @param moneyCurrencyCd the code of a currency the money is to be converted to.
     * @param amountToConvert the amount of money in the main currency that is to be converted.
     * @return an instance of the {@link ConverterResponseDTO} containing a amount of money converted to a given
     * currency or error messages if conversion does not succeed.
     * @author pcicenas
     * @since 2020-06
     */
    @GetMapping("convert")
    @ApiOperation(value = "Input the value to exchange", notes = "Returns an exchanged amount.")
    @ResponseStatus(HttpStatus.OK)
    public ConverterResponseDTO convertAmount(@ApiParam(value = "Main currency code") @Nullable @RequestParam String mainCurrencyCd,
                                              @ApiParam(value = "Money currency code") @Nullable @RequestParam String moneyCurrencyCd,
                                              @ApiParam(value = "Amount of money to convert (in Main currency")
                                              @RequestParam String amountToConvert) {
        return rateService.calculateAmount(mainCurrencyCd, moneyCurrencyCd, amountToConvert);
    }

    /**
     * Retrieves a rate for a given ISO currencies pair.
     *
     * @param mainCurrencyCd  the code of a currency the money is to be converted from.
     * @param moneyCurrencyCd the code of a currency the money is to be converted to.
     * @return an instance of the {@link ConverterResponseDTO} containing a calculated rate
     * or error messages if calculation does not succeed.
     * @author pcicenas
     * @since 2020-06
     */
    @GetMapping("rate")
    @ApiOperation(value = "Input codes for preferred currencies", notes = "Returns a rate for 2 currencies.")
    @ResponseStatus(HttpStatus.OK)
    public ConverterResponseDTO retrieveRate(@ApiParam(value = "Main currency code") @RequestParam String mainCurrencyCd,
                                             @ApiParam(value = "Money currency code") @RequestParam String moneyCurrencyCd) {

        return rateService.calculateRate(mainCurrencyCd, moneyCurrencyCd);
    }

    public RateService getRateService() {
        return rateService;
    }

    public void setRateService(RateService rateService) {
        this.rateService = rateService;
    }
}
