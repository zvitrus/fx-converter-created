package me.pcicenas.currency.fxconverter.dto;

import java.util.List;

/**
 * A DTO for REST response construction. Contains a value and error messages.
 *
 * @author pcicenas
 * @since 2020-06
 */
public class ConverterResponseDTO {

    /**
     * A value that is supposed to be returned.
     *
     * @author pcicenas
     * @since 2020-06
     */
    private String value;

    /**
     * A {@link List} of error messages.
     *
     * @author pcicenas
     * @since 2020-06
     */
    private List<String> errorMessages;


    public ConverterResponseDTO() {
    }

    public ConverterResponseDTO(String amount, List<String> errorMessages) {
        this.value = amount;
        this.errorMessages = errorMessages;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(List<String> errorMessages) {
        this.errorMessages = errorMessages;
    }
}
