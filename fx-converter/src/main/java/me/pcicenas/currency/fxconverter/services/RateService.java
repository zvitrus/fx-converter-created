package me.pcicenas.currency.fxconverter.services;

import me.pcicenas.currency.fxconverter.dto.ConverterResponseDTO;

public interface RateService {
    /**
     * Converts money from a given currency to another given currency.
     *
     * @param mainCurrencyCd  the code of a currency the money is to be converted from.
     * @param moneyCurrencyCd the code of a currency the money is to be converted to.
     * @param amountToConvert the amount of money in the main currency that is to be converted.
     * @return an instance of the {@link ConverterResponseDTO} containing a amount of money converted to a given
     * currency or error messages if conversion does not succeed.
     * @author pcicenas
     * @since 2020-06
     */
    ConverterResponseDTO calculateAmount(String mainCurrencyCd, String moneyCurrencyCd, String amountToConvert);

    /**
     * Calculates exchange rate  of a given ISO currency pair.
     *
     * @param mainCurrencyCd  the code of a currency the money is to be converted from.
     * @param moneyCurrencyCd the code of a currency the money is to be converted to.
     * @return an instance of the {@link ConverterResponseDTO} containing a calculated rate
     * or error messages if calculation does not succeed.
     * @author pcicenas
     * @since 2020-06
     */
    ConverterResponseDTO calculateRate(String mainCurrencyCd, String moneyCurrencyCd);
}
