package me.pcicenas.currency.fxconverter.services;

import com.google.common.collect.Lists;
import me.pcicenas.currency.fxconverter.dto.ConverterResponseDTO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

/**
 * An implementation of {@link RateService}. Responsible for operations related to money conversions from different
 * currencies.
 *
 * @author pcicenas
 * @since 2020-06
 */
public class RateServiceImpl implements RateService {

    private static final String AMOUNT_ERROR_MESSAGE = "The amount is not valid. It also must not be empty or negative.";
    private static final String CURRENCY_TYPE_ERROR_MESSAGE = "A currency type is unknown or has not been provided.";
    /**
     * Holds currencies and exchange rates. All the given exchange rates are provided denoting the amount of Danish
     * kroner (DKK) required to purchase 100 in the mentioned currency.
     */
    private Map<String, String> ratesOfCurrencies;

    /**
     * {@inheritDoc}
     *
     * @author pcicenas
     * @since 2020-06
     */
    @Override
    public ConverterResponseDTO calculateAmount(String mainCurrencyCd, String moneyCurrencyCd, String amountToConvert) {

        ConverterResponseDTO calculatedAmountResponseDTO = new ConverterResponseDTO();
        if (!isAmountValid(amountToConvert, calculatedAmountResponseDTO)) {
            if (calculatedAmountResponseDTO.getErrorMessages() == null) {
                calculatedAmountResponseDTO.setErrorMessages(Lists.newArrayList(AMOUNT_ERROR_MESSAGE));
            } else {
                calculatedAmountResponseDTO.getErrorMessages().add(AMOUNT_ERROR_MESSAGE);
            }
            return calculatedAmountResponseDTO;
        }

        ConverterResponseDTO rateResponseDTO = calculateRate(mainCurrencyCd, moneyCurrencyCd);

        BigDecimal convertedAmount = null;
        if (rateResponseDTO.getErrorMessages() == null) {
            convertedAmount = new BigDecimal(amountToConvert).multiply(new BigDecimal(rateResponseDTO
                    .getValue()));
        }

        calculatedAmountResponseDTO.setValue(convertedAmount == null ? null : convertedAmount.toString());
        calculatedAmountResponseDTO.setErrorMessages(rateResponseDTO.getErrorMessages());
        return calculatedAmountResponseDTO;
    }

    /**
     * Validates the provided amount of money that must not be negative or {@code null}.
     *
     * @param amount amount of money to validate.
     * @return {code true} if the amount is valid, {@link false} otherwise.
     * @author pcicenas
     * @since 2020-06
     */
    protected boolean isAmountValid(String amount, ConverterResponseDTO converterResponseDTO) {
        try {
            return !(amount == null || new BigDecimal(amount).compareTo(BigDecimal.ZERO) < 0);

        } catch (Exception e) {
            converterResponseDTO.setErrorMessages(Lists.newArrayList(e.getMessage()));
        }
        return false;
    }

    /**
     * {@inheritDoc}
     *
     * @author pcicenas
     * @since 2020-06
     */
    @Override
    public ConverterResponseDTO calculateRate(String mainCurrencyCd, String moneyCurrencyCd) {

        ConverterResponseDTO converterResponseDTO = new ConverterResponseDTO();
        if (!areCurrenciesValid(mainCurrencyCd, moneyCurrencyCd)) {
            converterResponseDTO.setErrorMessages(Lists.newArrayList(CURRENCY_TYPE_ERROR_MESSAGE));
        }

        if (converterResponseDTO.getErrorMessages() == null) {
            converterResponseDTO.setValue(
                    new BigDecimal(ratesOfCurrencies.get(mainCurrencyCd))
                            .divide(new BigDecimal(ratesOfCurrencies.get(moneyCurrencyCd)), 5, RoundingMode.HALF_DOWN).toString());
        }

        return converterResponseDTO;
    }

    /**
     * Validates a provided ISO currency pair.
     *
     * @param mainCurrencyCd  the code of a currency the money is to be converted from.
     * @param moneyCurrencyCd the code of a currency the money is to be converted to.
     * @return {code true} if the codes are valid, {@link false} otherwise.
     * @author pcicenas
     * @since 2020-06
     */
    protected boolean areCurrenciesValid(String mainCurrencyCd, String moneyCurrencyCd) {
        return mainCurrencyCd != null && moneyCurrencyCd != null
                && ratesOfCurrencies.containsKey(mainCurrencyCd) && ratesOfCurrencies.containsKey(moneyCurrencyCd);
    }

    public Map<String, String> getRatesOfCurrencies() {
        return ratesOfCurrencies;
    }

    public void setRatesOfCurrencies(Map<String, String> ratesOfCurrencies) {
        this.ratesOfCurrencies = ratesOfCurrencies;
    }
}
