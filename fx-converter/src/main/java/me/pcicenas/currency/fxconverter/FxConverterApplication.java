package me.pcicenas.currency.fxconverter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@ImportResource("classpath:application-context.xml")
public class FxConverterApplication {

	public static void main(String[] args) {
		SpringApplication.run(FxConverterApplication.class, args);
	}

}
