import React, { Component } from "react";

const App = props => {
  return <React.Fragment>{props.children}</React.Fragment>;
};

export default App;
