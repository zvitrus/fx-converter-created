import React, { Component } from "react";
import axios from "axios";
import FxConverterComponent from "./FxConverterComponent";

class FxConverterContainer extends Component {
  state = {
    mainCurrencyCd: "DKK",
    moneyCurrencyCd: "DKK",
    amountToConvert: "",
    convertedAmount: "",
    errorMessages: []
  };

  handleConvert = e => {
    axios({
      url: "/converter/convert",
      method: "get",
      params: {
        mainCurrencyCd: this.state.mainCurrencyCd,
        moneyCurrencyCd: this.state.moneyCurrencyCd,
        amountToConvert: this.state.amountToConvert
      }
    })
      .then(response => {
        this.setState({ convertedAmount: response.data.value });
        this.setState({ errorMessages: response.data.errorMessages });
        console.log(
          "RESPONSE convertedAmount in state >>> " + this.state.convertedAmount
        );
      })
      .catch(function(error) {
        //it works without catch block as well
        console.log(error);
        if (error.response) {
          //HTTP error happened
          console.log(
            "An error. HTTP error/status code=",
            error.response.status
          );
        } else {
          //some other error happened
          console.log("An error. HTTP error/status code=", error.message);
        }
      });
  };

  handleMainCurrencySelect = e => {
    console.log("$$$$$$$ e.target.value>>>>>>", e.target.value);
    let mainCurrencyCd = e.target.value;
    this.setState({ mainCurrencyCd: mainCurrencyCd });
  };

  handleMoneyCurrencySelect = e => {
    let moneyCurrencyCd = e.target.value;
    this.setState({ moneyCurrencyCd: moneyCurrencyCd });
    console.log("$$$$$$$ e.target.value>>>>>>", e.target.value);
  };

  handleAmount = e => {
    let amountToConvert = e.target.value;
    this.setState({ amountToConvert: amountToConvert });
    console.log("$$$$$$$ e.target.value>>>>>>", e.target.value);
  };

  render() {
    return (
      <FxConverterComponent
        onConvert={this.handleConvert}
        // onFile={this.handleFile}
        onAmount={this.handleAmount}
        onMainCurrencySelect={this.handleMainCurrencySelect}
        onMoneyCurrencySelect={this.handleMoneyCurrencySelect}
        convertedAmount={this.state.convertedAmount}
        errorMessages={this.state.errorMessages}
      />
    );
  }
}

export default FxConverterContainer;
