import React, { Component } from "react";
import PropTypes from "prop-types";

const FxConverterComponent = props => {
  return (
    <div className="container-fluid">
      <h3>Currency Converter</h3>
      <form>
        <div className="">
          <label className="m-2">Main Currency</label>
          <select
            onChange={props.onMainCurrencySelect}
            className="form-control col-2 m-2"
            id="documentTypeSelect"
          >
            <option>DKK</option>
            <option>EUR</option>
            <option>USD</option>
            <option>GBP</option>
            <option>SEK</option>
            <option>NOK</option>
            <option>CHF</option>
            <option>JPY</option>
          </select>
          <label className="m-2">Money Currency</label>
          <select
            onChange={props.onMoneyCurrencySelect}
            className="form-control col-2 m-2"
            id="documentTypeSelect"
          >
            <option>DKK</option>
            <option>EUR</option>
            <option>USD</option>
            <option>GBP</option>
            <option>SEK</option>
            <option>NOK</option>
            <option>CHF</option>
            <option>JPY</option>
          </select>
        </div>

        <label className="m-2 ">Amount</label>
        <br />
        <input
          className="m-2 form-control col-2"
          type="text"
          onChange={props.onAmount}
        />
        <label className="m-2 ">Converted amount: </label>
        <span className="italic-style-small">{props.convertedAmount}</span>
        <span className="italic-style-small">{props.errorMessages}</span>
        <br />

        <button
          type="button"
          className="btn btn-info m-2"
          onClick={props.onConvert}
        >
          Convert
        </button>
      </form>
    </div>
  );
};

export default FxConverterComponent;
