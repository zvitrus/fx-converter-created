import React from "react";
import ReactDOM from "react-dom";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { Switch, Route } from "react-router";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import FxConverterContainer from "./components/fx-converter/FxConverterContainer";

ReactDOM.render(
  <BrowserRouter>
    <App>
      <Switch>
        <Route exact path="/" component={FxConverterContainer} />
      </Switch>
    </App>
  </BrowserRouter>,
  document.getElementById("root")
);
